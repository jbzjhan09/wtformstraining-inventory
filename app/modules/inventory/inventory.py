from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session
from datetime import datetime
from app.modules.inventory.inventory_form import InventoryForm
from app.modules.inventory.cart_form import CartForm

inventory_blueprint = Blueprint('inventory', __name__)

@inventory_blueprint.route('/inventory')
def product_list():
	cart_form = CartForm()
	inventory = g.inventorydb.getInventory()
	cart = g.cart.getCart(session['username'])
	total = 0
	for c in cart:
		total += c['sum'] * c['inventory_details']['price']
	cart = g.cart.getCart(session['username'])
	return render_template('/inventory/inventory.html', inventory=inventory, cart=cart, cart_form = cart_form, total=total)

@inventory_blueprint.route('/add', methods=['GET', 'POST'])
def add_product():
	form = InventoryForm()
	if request.method == 'POST':
		if form.validate() == True:
			name = request.form['name']
			price = float(request.form['price'])
			g.inventorydb.insertInventory(name, price)
			flash('Product successfully added to the inventory', 'message_success')
		else:
			flash('Please fill up the form correctly', 'message_failure')
	return render_template('/inventory/add_inventory.html', form=form)

#@inventory_blueprint.route('/add', methods=['POST'])
#def add_product():
#	if form.validate == False:
#		flash('Please fill up the form correctly.','message')
#	flash('Product successfully added to the inventory', 'message')
#	return render_template('/inventory/inventory.html', form=form)