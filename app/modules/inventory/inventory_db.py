from bson.objectid import ObjectId

class InventoryDB:
	def __init__(self, conn):
		self.conn = conn

	def getInventory(self):
		return self.conn.find();

	def getInventoryById(self, inventory_id):
		return list(self.conn.find({'_id': ObjectId(inventory_id)}))

	def insertInventory(self, name, price):
		self.conn.insert({'name': name, 'price': price})