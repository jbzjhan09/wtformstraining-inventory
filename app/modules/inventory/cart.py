from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session
from datetime import datetime
from app.modules.inventory.cart_form import CartForm

cart_blueprint = Blueprint('cart', __name__)

@cart_blueprint.route('/', methods=['POST'])
def push_to_cart():
	cart_form = CartForm()
	if request.method == 'POST':
		try:
			quantity = float(request.form['quantity'])
		except:
			flash('Please enter a valid number', 'message_failure')
			return redirect('/inventory')
		if cart_form.validate() == False:
			flash('Please fill up quantity field.', 'message_failure')
			return redirect('/inventory')
		inventory_id = request.form['inventory_id']
		g.cart.insertCart(inventory_id, quantity, session['username']);
		flash('Product added to cart.', 'message_success')
		return redirect('/inventory')

@cart_blueprint.route('/cart', methods=['GET'])
def goToCart():
	cart = g.cart.getCart(session['username'])
	total = 0
	for c in cart:
		total += c['sum'] * c['inventory_details']['price']
	cart = g.cart.getCart(session['username'])
	return render_template('/inventory/cart.html', cart=cart, total=total)