from flask_wtf import Form
from wtforms import TextField, DecimalField, SubmitField
from wtforms import validators, ValidationError

class InventoryForm(Form):
	name = TextField('Product Name:', [validators.Required('Product name is required.')])
	price = DecimalField('Price', [validators.Required('Price is required.')])
	submit = SubmitField()